=== Construction Landing Page ===

Author: Rara Theme (http://raratheme.com)

Tags: two-columns, one-column, right-sidebar, custom-menu, custom-logo, featured-images, sticky-post, threaded-comments, translation-ready, custom-background, full-width-template, theme-options, portfolio, post-formats, blog, e-commerce

Requires at least: 4.0
Tested up to: 4.7.3
Stable tag: 1.0.7
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Construction Landing Page is a multipurpose, mobile friendly WordPress Theme for business, freelancers and private use. The Theme has a professional design and great features to suit a Construction Business. Although designed with Construction Industry in mind, the theme is very flexible and versatile to be used by various types of freelancers, corporations, institutions and businesses. Construction Landing Page comes with several features to make user-friendly, interactive and visually stunning website. Such features include custom menu with Call to Action Button, attractive banner with contact form, Services section, Portfolio section, Client Section, testimonial section, Banner with Call to Action Button (CTA), and social media. The theme is SEO friendly with optimized codes, which make it easy for your site to rank on Google and other search engines. The theme is rigorously tested and optimized for speed and faster page load time and has a secure and clean code. The theme is also translation ready. Designed with visitor engagement in mind, Construction Landing Page helps you to easily and intuitively create professional and appealing websites. Check the demo at http://raratheme.com/preview/construction-landing-page/, documentation at http://raratheme.com/documentation/construction-landing-page/, and get support at http://raratheme.com/support-ticket/.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Documentation ==
    * Home Page Setup
    * To set up the homepage you must first create a new page.
        => Create a Home Page
        => Go to Pages > Add New.
        => Enter the Page Title for the Page.
        => Enter the Page Content.
        => Set featured Image for the home page by clicking on Set Featured Image. The selected Image will then Appear on the Homepage below the Primary menu. The recommended size for the image is 1915*720 pixels.
        => Select the Homepage Template from the Page Attributes section.
        => Click Publish.
        
    * Configuring the front Page display setting
        => Go to Appearance>Customize > Default Settings> Static Front Page.
        => Select A static page under Front Page displays
        => Choose the page you just created with homepage template as Front page.
        => Click Save & Publish.
        
    * Banner Section:
        => The Featured Image of  the Page ( with Homepage Template) selected as  Front page will appear as a Banner Image if banner is enabled.
        => The Title and content of the page of the Homepage will appear on the banner
        => You need to Install and activate Contact Form 7  Plugin to see options relating to contact form
        => Go to Appearance> Customize> Homepage Settings>Banner Section
        => Check to Enable Banner Section.
        => For Options relating to contact form for   Install and activate Contact Form 7  
        => Enter the Short code of the form
        => Click Save and Publish. 
    
    Detailed Documentation can be found here https://raratheme.com/documentation/construction-landing-page/


== Copyrights and License ==

Unless otherwise specified, all the theme files, scripts and images are licensed under GPLv2 or later


External resources linked to the theme.

* PT Sans Font by through Google Font
https://www.google.com/fonts/specimen/PT+Sans
*Font Awesome
https://fortawesome.github.io/Font-Awesome/

# Images
All images are under Creative Commons Public Domain deed CC0.
https://pixabay.com/en/construction-workers-laborers-759298/
https://pixabay.com/en/construction-worker-building-job-642631/
https://pixabay.com/en/sky-clouds-construction-brick-layer-78113/
https://pixabay.com/en/construction-welding-welder-857143/
https://pixabay.com/en/building-windows-business-1081868/
https://pixabay.com/en/construction-workers-installing-646465/
https://pixabay.com/en/steelworkers-concrete-formwork-1029665/
https://pixabay.com/en/construction-worker-building-652292/
https://pixabay.com/en/men-working-construction-sawing-978395/
https://pixabay.com/en/construction-worker-band-saw-labor-664679/
https://pixabay.com/en/construction-construction-workers-1181982/
https://pixabay.com/en/portrait-boy-man-smile-forest-1130764/
https://pixabay.com/en/girl-portrait-smile-beauty-winter-1170136/
https://pixabay.com/en/construction-worker-building-652292/
https://pixabay.com/en/construction-site-crane-pier-1156567/
http://pixabay.com/en/workers-road-workers-site-work-1210670/
https://pixabay.com/en/excavators-site-construction-work-1178241/
https://pixabay.com/en/scaffold-roof-tiles-repair-1207389/
https://pixabay.com/en/taps-building-construction-956497/
https://pixabay.com/en/man-male-portrait-beard-stylish-1122364/


# JS
All the JS are licensed under GPLv2 or later
1) backgroun-stretch: http://srobbin.com/jquery-plugins/backstretch/
2) matchHeight : https://github.com/liabru/jquery-match-height
3) responsive menu: https://www.berriart.com/sidr/

* Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)


* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)


All other resources and theme elements are licensed under the GPLv2 or later


Construction Landing Page WordPress Theme, Copyright Rara Theme 2015, Raratheme.com
Construction Landing Page WordPress Theme is distributed under the terms of the GPLv2 or later


*  This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

== Change Log ==
   1.0.7
   * Fixed backgrond issue in home page section for iphone.
   * Changed equal height js with match height js and fixed overflow issue in service section. 
   
   1.0.6
   * Added pro section in customizer 
   
   1.0.5
   * Made theme woocommerce compatible.   

   1.0.4
   * Fixed minor design issues.

   1.0.3
   * Bug fixes.

   1.0.2
   * Removed unnecessary files.
   * Fixed design issues.

   1.0.1
   * Bug fixes.
    
   1.0.0
   * Initial Release