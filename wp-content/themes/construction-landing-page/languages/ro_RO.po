msgid ""
msgstr ""
"Project-Id-Version: Construction Landing Page\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-08 11:09+0000\n"
"PO-Revision-Date: 2017-06-02 07:52+0000\n"
"Last-Translator: nicu.ampoitan <nicu.uma@gmail.com>\n"
"Language-Team: Romanian\n"
"Language: ro-RO\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 :(((n%100>19)||(( n%100==0)&&(n!"
"=0)))? 2 : 1))\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco - https://localise.biz/"

#: 404.php:17
msgid ""
"The Page you are looking for is no longer exists. Please use search or "
"navigate from Homepage."
msgstr ""
"Pagina pe care o căutați nu mai există. Utilizați căutarea sau navigați pe "
"pagina de pornire."

#: 404.php:18
msgid "Back to homepage"
msgstr "Înapoi Homepage"

#: archive.php:38 index.php:46 search.php:38
msgid "Page"
msgstr ""

#: comments.php:57
msgid "Comments"
msgstr ""

#: comments.php:87
msgid "Comment navigation"
msgstr ""

#: comments.php:93
msgid "Older Comments"
msgstr ""

#: comments.php:95
msgid "Newer Comments"
msgstr ""

#: comments.php:121
msgid "Comments are closed."
msgstr ""

#: comments.php:151
msgid "Required fields are marked"
msgstr ""

#: comments.php:161
msgid "Name*"
msgstr ""

#: comments.php:169
msgid "Email*"
msgstr ""

#: comments.php:177
msgid "Website"
msgstr ""

#: comments.php:197
msgid "Leave a Reply"
msgstr ""

#: comments.php:199
#, php-format
msgid "Leave a Reply to %s"
msgstr ""

#: comments.php:201
msgid "Cancel Reply"
msgstr ""

#: comments.php:203
msgid "Post Comment"
msgstr ""

#: comments.php:211
msgid "Comment"
msgstr ""

#: comments.php:221
#, php-format
msgid "You must be <a href=\"%s\">logged in</a> to post a comment."
msgstr ""

#: comments.php:233
#, php-format
msgid ""
"Logged in as <a href=\"%1$s\">%2$s</a>. <a href=\"%3$s\" title=\"Log out of "
"this account\">Log out?</a>"
msgstr ""

#: comments.php:247
msgid "Your email address will not be published. "
msgstr ""

#: footer.php:87
msgid "&copy; Copyright "
msgstr ""

#: footer.php:87 inc/extras.php:223 inc/extras.php:223 inc/extras.php:224
#: inc/extras.php:228 inc/extras.php:228 inc/extras.php:232
msgid "Y"
msgstr ""

#: footer.php:91
msgid "Construction Landing Page Theme by: Rara Theme"
msgstr ""

#: footer.php:93
msgid "https://wordpress.org/"
msgstr ""

#: footer.php:93
#, php-format
msgid "Powered by %s"
msgstr ""

#: functions.php:54
msgid "Primary"
msgstr ""

#: functions.php:148
msgid "Right Sidebar"
msgstr ""

#: functions.php:150 functions.php:160 functions.php:170 functions.php:180
msgid "Add widgets here."
msgstr ""

#: functions.php:158
msgid "Footer Sidebar First"
msgstr ""

#: functions.php:168
msgid "Footer Sidebar Second"
msgstr ""

#: functions.php:178
msgid "Footer Sidebar Third"
msgstr ""

#: search.php:15
msgid "If you didn&rsquo;t find what you were looking for, try a new search!"
msgstr ""

#. Name of the template
msgid "Home Page"
msgstr ""

#: inc/customizer.php:35
msgid "Choose Post"
msgstr ""

#: inc/customizer.php:51
msgid "Choose Post/Page"
msgstr ""

#: inc/customizer.php:67
msgid "Choose Page"
msgstr ""

#: inc/customizer.php:91
msgid "Default Settings"
msgstr ""

#: inc/customizer.php:93
msgid "Default section provided by wordpress customizer"
msgstr ""

#: inc/customizer.php:131
msgid "Header Setting"
msgstr ""

#: inc/customizer.php:167
msgid "Phone Number"
msgstr ""

#: inc/customizer.php:191
msgid "Home Page Settings"
msgstr ""

#: inc/customizer.php:193
msgid "Customize Home Page Settings"
msgstr ""

#: inc/customizer.php:209
msgid "Banner Section"
msgstr ""

#: inc/customizer.php:245
msgid "Enable Banner Section"
msgstr ""

#: inc/customizer.php:247
msgid ""
"Check to enable banner on the front page. The featured image and content of "
"Static Front Page will be displayed in the banner section. To add or edit "
"the content of Static Front Page, go to Dashboard >> Pages > All Pages  and "
"edit the page with Front Page template."
msgstr ""

#: inc/customizer.php:285 inc/customizer.php:2269
msgid "Contact Form"
msgstr ""

#: inc/customizer.php:289 inc/customizer.php:2273
msgid ""
"Enter the Contact Form Shortcode. Ex. [contact-form-7 id=\"186\" "
"title=\"Google contact\"]"
msgstr ""

#: inc/customizer.php:309
msgid "About Section"
msgstr ""

#: inc/customizer.php:345
msgid "Enable About Section"
msgstr ""

#: inc/customizer.php:381 inc/customizer.php:597 inc/customizer.php:765
#: inc/customizer.php:1093 inc/customizer.php:1977 inc/customizer.php:2227
msgid "Select Page"
msgstr ""

#: inc/customizer.php:383 inc/customizer.php:385 inc/customizer.php:599
#: inc/customizer.php:767 inc/customizer.php:1095 inc/customizer.php:1979
#: inc/customizer.php:2229
msgid ""
"Title and description of selected page will display as section title and "
"description."
msgstr ""

#: inc/customizer.php:425 inc/customizer.php:805 inc/customizer.php:1133
#: inc/customizer.php:2017
msgid "Select Post/Page One"
msgstr ""

#: inc/customizer.php:463 inc/customizer.php:843 inc/customizer.php:1171
#: inc/customizer.php:2055
msgid "Select Post/Page Two"
msgstr ""

#: inc/customizer.php:501 inc/customizer.php:881 inc/customizer.php:1209
#: inc/customizer.php:2093
msgid "Select Post/Page Three"
msgstr ""

#: inc/customizer.php:523
msgid "Promotional Block Section"
msgstr ""

#: inc/customizer.php:559
msgid "Enable Promotional Block Section"
msgstr ""

#: inc/customizer.php:637
msgid "CTA Button Label"
msgstr ""

#: inc/customizer.php:673
msgid "CTA Button Link"
msgstr ""

#: inc/customizer.php:693
msgid "Portfolio Section"
msgstr ""

#: inc/customizer.php:729
msgid "Enable Portfolio Section"
msgstr ""

#: inc/customizer.php:919 inc/customizer.php:1247 inc/customizer.php:2131
msgid "Select Post/Page Four"
msgstr ""

#: inc/customizer.php:957 inc/customizer.php:1285
msgid "Select Post/Page Five"
msgstr ""

#: inc/customizer.php:995 inc/customizer.php:1323
msgid "Select Post/Page Six"
msgstr ""

#: inc/customizer.php:1019
msgid "Services Section"
msgstr ""

#: inc/customizer.php:1055
msgid "Enable Services Section"
msgstr ""

#: inc/customizer.php:1361
msgid "Select Post/Page Seven"
msgstr ""

#: inc/customizer.php:1399
msgid "Select Post/Page Eight"
msgstr ""

#: inc/customizer.php:1423
msgid "Clients Section"
msgstr ""

#: inc/customizer.php:1459
msgid "Enable Clients Section"
msgstr ""

#: inc/customizer.php:1495
msgid "Section Title"
msgstr ""

#: inc/customizer.php:1535
msgid "Upload a logo (One)"
msgstr ""

#: inc/customizer.php:1573
msgid "Logo Url (One)"
msgstr ""

#: inc/customizer.php:1613
msgid "Upload a logo (Two)"
msgstr ""

#: inc/customizer.php:1651
msgid "Logo Url (Two)"
msgstr ""

#: inc/customizer.php:1691
msgid "Upload a logo (Three)"
msgstr ""

#: inc/customizer.php:1729
msgid "Logo Url (Three)"
msgstr ""

#: inc/customizer.php:1769
msgid "Upload a logo (Four)"
msgstr ""

#: inc/customizer.php:1807
msgid "Logo Url (Four)"
msgstr ""

#: inc/customizer.php:1847
msgid "Upload a logo (Five)"
msgstr ""

#: inc/customizer.php:1885
msgid "Logo Url (Five)"
msgstr ""

#: inc/customizer.php:1905
msgid "Testimonials Section"
msgstr ""

#: inc/customizer.php:1941
msgid "Enable Testimonials Section"
msgstr ""

#: inc/customizer.php:2155
msgid "Contact Form Section"
msgstr ""

#: inc/customizer.php:2191
msgid "Enable Contact Form Section"
msgstr ""

#: inc/customizer.php:2293
msgid "Breadcrumb Settings"
msgstr ""

#: inc/customizer.php:2331
msgid "Enable Breadcrumb"
msgstr ""

#: inc/customizer.php:2367
msgid "Show current"
msgstr ""

#: inc/customizer.php:2387 inc/extras.php:170
msgid "Home"
msgstr ""

#: inc/customizer.php:2403
msgid "Breadcrumb Home Text"
msgstr ""

#: inc/customizer.php:2423 inc/extras.php:169
msgid ">"
msgstr ""

#: inc/customizer.php:2439
msgid "Breadcrumb Separator"
msgstr ""

#: inc/extras.php:123
#, php-format
msgid "Search Results for: \"%s\""
msgstr ""

#: inc/extras.php:146
msgid "404 - Page not found"
msgstr "404 - Pagina negăsită"

#: inc/extras.php:220
msgid "Search Results for \""
msgstr ""

#: inc/extras.php:220
msgid "\""
msgstr ""

#: inc/extras.php:224
msgid "m"
msgstr ""

#: inc/extras.php:224 inc/extras.php:229
msgid "F"
msgstr ""

#: inc/extras.php:225
msgid "d"
msgstr ""

#: inc/extras.php:286
#, php-format
msgid "Page %s"
msgstr ""

#: inc/extras.php:328
msgid "404 Error - Page not Found"
msgstr ""

#: inc/extras.php:364
msgid "About Author"
msgstr ""

#: inc/info.php:11
msgid "Information Links"
msgstr ""

#: inc/info.php:29
msgid ""
"<strong>Instruction for how to setup Home Page in Construction Landing Page "
"Theme</strong><br/>1. Go to Pages and create a new page (Title can be "
"anything. For example, Home )<br/>\n"
"2. In right column and under Page Attributes, choose \"Home Page\" "
"template<br/>\n"
"3. Click on Publish<br/>\n"
"4. Go to Appearance-> Customize -> Default Settings -> Static Front Page<br/>"
"\n"
"5. Select A static page<br/>\n"
"6. Under Front Page, select the page that you created in the step 1<br/>\n"
"7. Save changes"
msgstr ""

#: inc/info.php:47
msgid "Need help?"
msgstr ""

#: inc/info.php:48
msgid "View demo"
msgstr ""

#: inc/info.php:48 inc/info.php:49 inc/info.php:50 inc/info.php:51
#: inc/info.php:52 inc/info.php:53
msgid "here"
msgstr ""

#: inc/info.php:49
msgid "View documentation"
msgstr ""

#: inc/info.php:50
msgid "Theme info"
msgstr ""

#: inc/info.php:51
msgid "Support ticket"
msgstr ""

#: inc/info.php:52
msgid "Rate this theme"
msgstr ""

#: inc/info.php:53
msgid "More WordPress Themes"
msgstr ""

#: inc/info.php:57
msgid "About Construction Landing Page"
msgstr ""

#: inc/info.php:178
msgid "Pro Available"
msgstr ""

#: inc/info.php:180
msgid "VIEW PRO THEME"
msgstr ""

#. $id
#: inc/metabox.php:25
msgid "Sidebar Layout"
msgstr ""

#: inc/metabox.php:45
msgid "Right sidebar (default)"
msgstr ""

#: inc/metabox.php:55
msgid "No sidebar"
msgstr ""

#: inc/metabox.php:79
msgid "Choose Sidebar Template"
msgstr ""

#: inc/template-tags.php:23
#, php-format
msgctxt "post date"
msgid "Posted on %s"
msgstr ""

#: inc/template-tags.php:28
#, php-format
msgctxt "post author"
msgid "Posted by %s"
msgstr ""

#. translators: used between list items, there is a space after the comma
#. translators: used between list items, there is a space after the comma
#: inc/template-tags.php:36 inc/template-tags.php:42
msgid ", "
msgstr ""

#: inc/template-tags.php:38 inc/template-tags.php:44
#, php-format
msgid "%1$s"
msgstr ""

#. translators: %s: post title
#: inc/template-tags.php:53
#, php-format
msgid "Leave a Comment<span class=\"screen-reader-text\"> on %s</span>"
msgstr ""

#. translators: %s: Name of current post
#: inc/template-tags.php:69
#, php-format
msgid "Edit %s"
msgstr ""

#. Base ID
#: inc/widget-featured-post.php:49
msgid "RARA: Featured Post"
msgstr ""

#: inc/widget-featured-post.php:51
msgid "A Featured Post Widget"
msgstr ""

#: inc/widget-featured-post.php:79 inc/widget-featured-post.php:193
#: inc/widget-featured-post.php:277
msgid "Read More"
msgstr ""

#: inc/widget-featured-post.php:169
msgid "--choose--"
msgstr ""

#: inc/widget-featured-post.php:205
msgid "Posts"
msgstr ""

#: inc/widget-featured-post.php:225
msgid "Read More Text"
msgstr ""

#: inc/widget-featured-post.php:237 inc/widget-popular-post.php:235
#: inc/widget-recent-post.php:233
msgid "Show Post Thumbnail"
msgstr ""

#. Base ID
#: inc/widget-popular-post.php:49
msgid "RARA: Popular Post"
msgstr ""

#: inc/widget-popular-post.php:51
msgid "A Popular Post Widget"
msgstr ""

#: inc/widget-popular-post.php:213 inc/widget-recent-post.php:211
#: inc/widget-social-links.php:191
msgid "Title"
msgstr ""

#: inc/widget-popular-post.php:223 inc/widget-recent-post.php:221
msgid "Number of Posts"
msgstr ""

#: inc/widget-popular-post.php:245 inc/widget-recent-post.php:243
msgid "Show Post Date"
msgstr ""

#. Base ID
#: inc/widget-recent-post.php:49
msgid "RARA: Recent Post"
msgstr ""

#: inc/widget-recent-post.php:51
msgid "A Recent Post Widget"
msgstr ""

#. Base ID
#: inc/widget-social-links.php:49
msgid "RARA: Social Links"
msgstr ""

#: inc/widget-social-links.php:51
msgid "A Social Links Widget"
msgstr ""

#: inc/widget-social-links.php:109 inc/widget-social-links.php:201
msgid "Facebook"
msgstr ""

#: inc/widget-social-links.php:113 inc/widget-social-links.php:211
msgid "Twitter"
msgstr ""

#: inc/widget-social-links.php:117 inc/widget-social-links.php:221
msgid "Pinterest"
msgstr ""

#: inc/widget-social-links.php:121
msgid "Linkedin"
msgstr ""

#: inc/widget-social-links.php:125 inc/widget-social-links.php:241
msgid "Google Plus"
msgstr ""

#: inc/widget-social-links.php:129 inc/widget-social-links.php:251
msgid "Instagram"
msgstr ""

#: inc/widget-social-links.php:133 inc/widget-social-links.php:261
msgid "YouTube"
msgstr ""

#: inc/widget-social-links.php:231
msgid "LinkedIn"
msgstr ""

#: inc/woocommerce-functions.php:71
msgid "Shop Sidebar"
msgstr ""

#: inc/woocommerce-functions.php:75
msgid "Sidebar displaying only in woocommerce pages."
msgstr ""

#: sections/portfolio.php:115
msgid "View Detail"
msgstr "Vezi Detalii"

#: template-parts/content-none.php:14
msgid "Nothing Found"
msgstr "Nu s-a găsit nimic"

#: template-parts/content-none.php:21
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: template-parts/content-none.php:28
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr "Se pare că nu găsim ceea ce căutați. Poate căutarea poate ajuta."

#: template-parts/content-page.php:28 template-parts/content-single.php:44
#: template-parts/content.php:50
msgid "Pages:"
msgstr ""

#: template-parts/content-single.php:39 template-parts/content.php:44
#, php-format
msgid "Continue reading %s <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: template-parts/content.php:58
msgid "Continue Reading"
msgstr "Continua"

#. Name of the theme
msgid "Construction Landing Page"
msgstr ""

#. Description of the theme
msgid ""
"Construction Landing Page is a multipurpose, mobile friendly WordPress Theme "
"for business, freelancers and private use. The Theme has a professional "
"design and great features to suit a Construction Business. Although designed "
"with Construction Industry in mind, the theme is very flexible and versatile "
"to be used by various types of freelancers, corporations, institutions and "
"businesses. Construction Landing Page comes with several features to make "
"user-friendly, interactive and visually stunning website. Such features "
"include custom menu with Call to Action Button, attractive banner with "
"contact form, Services section, Portfolio section, Client Section, "
"testimonial section, Banner with Call to Action Button (CTA), and social "
"media. The theme is SEO friendly with optimized codes, which make it easy "
"for your site to rank on Google and other search engines. The theme is "
"rigorously tested and optimized for speed and faster page load time and has "
"a secure and clean code. The theme is also translation ready. Designed with "
"visitor engagement in mind, Construction Landing Page helps you to easily "
"and intuitively create professional and appealing websites. Check the demo "
"at http://raratheme.com/preview/construction-landing-page/, documentation at "
"http://raratheme.com/documentation/construction-landing-page/, and get "
"support at http://raratheme.com/support-ticket/."
msgstr ""

#. URI of the theme
msgid "http://raratheme.com/wordpress-themes/construction-landing-page/"
msgstr ""

#. Author of the theme
msgid "Rara Theme"
msgstr ""

#. Author URI of the theme
msgid "http://raratheme.com/"
msgstr ""
