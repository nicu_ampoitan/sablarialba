<?php /* Template Name: Portofolio */ ?>

<?php get_header(); ?>

	<div id="portofolio-page" class="content-area">

		<main id="main" class="site-main" role="main">

                    <section id="our-projects-section" class="our-projects">

                        <div class="container">

                            <?php 

                                $qry = new WP_Query( array( 

                                    'post_type'           => array( 'post', 'page' ),

                                    'posts_per_page'      => -1,
                                    
                                    'post_parent'         => 31,

                                ) );


                                if( $qry->have_posts() ){ ?>

                                    <div class="row">

                                    <?php 

                                        while( $qry->have_posts() ){

                                            $qry->the_post();?>

                                            <div class="col">

                                                <div class="img-holder">

                                                    <?php 

                                                    if( has_post_thumbnail() ){

                                                        the_post_thumbnail( 'construction-landing-page-about-portfolio' );

                                                    }else{ ?>

                                                        <img src="<?php echo get_template_directory_uri(); ?>/images/no-preview.png" > 

                                                    <?php 

                                                    } ?>

                                                    <div class="text-holder">

                                                        <div class="table">

                                                                <div class="table-row">

                                                                        <div class="table-cell">

                                                                                <h3 class="title"><?php the_title(); ?></h3>

                                                                                <?php the_excerpt(); ?>

                                                                                <a href="<?php the_permalink(); ?>" class="btn-more"><?php esc_html_e( 'View Detail','construction-landing-page' ); ?></a>

                                                                        </div>

                                                                </div>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                       <?php 

                                       }

                                       wp_reset_postdata(); 

                                    ?>

                                    </div>

                                <?php 

                                } 

                            ?>

                        </div>

                    </section>

		</main><!-- #main -->

	</div><!-- #primary -->

<?php

get_footer();